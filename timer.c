#include <bcm2835.h>
#include <pthread.h>
#include <unistd.h>
#include <signal.h>
#include <sys/time.h>
 
#define PIN1 RPI_GPIO_P1_11
#define PIN2 RPI_GPIO_P1_12
#define PIN3 RPI_GPIO_P1_24
#define PIN4 RPI_GPIO_P1_15
#define PIN5 RPI_GPIO_P1_16
#define PIN6 RPI_GPIO_P1_18
#define PIN7 RPI_GPIO_P1_22
#define PIN8 RPI_GPIO_P1_07
 
void sig_handler( int signo );
void set_pins_output( void );
void set_all_pin_low( void );
void timer_seconds( long int seconds );
void timer_useconds( long int useconds );
 
int main(){

 
	if (!bcm2835_init())
		return 1;
	set_pins_output();
	set_all_pin_low();
 
	signal( SIGALRM, sig_handler );
	timer_seconds( 2 );
 
	while ( 1 );  
 	
	bcm2835_close();
	return 0;
}
 
void timer_seconds( long int seconds ){

	struct itimerval timer1;
 
	timer1 . it_interval . tv_usec = 0;
	timer1 . it_interval . tv_sec = seconds;
	timer1 . it_value . tv_usec = 0;
	timer1 . it_value . tv_sec = seconds;
 
	setitimer ( ITIMER_REAL, &timer1, NULL );
	//setitimer is classical Unix. better to use POSIX
	//timer_create(), timer_settime()
}

void timer_useconds ( long int useconds ){

	struct itimerval timer2;
 
	timer2 . it_interval . tv_usec = useconds;
	timer2 . it_interval . tv_sec = 0;
	timer2 . it_value . tv_usec = useconds;
	timer2 . it_value . tv_sec = 0;
 
	setitimer ( ITIMER_REAL, &timer2, NULL );
}

void set_all_pin_low ( void ){

	bcm2835_gpio_write(PIN1, LOW);
	bcm2835_gpio_write(PIN2, LOW);
	bcm2835_gpio_write(PIN3, LOW);
	bcm2835_gpio_write(PIN4, LOW);
	bcm2835_gpio_write(PIN5, LOW);
	bcm2835_gpio_write(PIN6, LOW);
	bcm2835_gpio_write(PIN7, LOW);
	bcm2835_gpio_write(PIN8, LOW);
}

void set_pins_output ( void ){

	bcm2835_gpio_fsel(PIN1, BCM2835_GPIO_FSEL_OUTP);
	bcm2835_gpio_fsel(PIN2, BCM2835_GPIO_FSEL_OUTP);
	bcm2835_gpio_fsel(PIN3, BCM2835_GPIO_FSEL_OUTP);
	bcm2835_gpio_fsel(PIN4, BCM2835_GPIO_FSEL_OUTP);
	bcm2835_gpio_fsel(PIN5, BCM2835_GPIO_FSEL_OUTP);
	bcm2835_gpio_fsel(PIN6, BCM2835_GPIO_FSEL_OUTP);
	bcm2835_gpio_fsel(PIN7, BCM2835_GPIO_FSEL_OUTP);
	bcm2835_gpio_fsel(PIN8, BCM2835_GPIO_FSEL_OUTP);
}

void sig_handler ( int signo ){

	static int i = 0;

	set_all_pin_low ();
	bcm2835_delay( 50 );
 
	i ++;
 
	switch ( i ){

		case  1:
			bcm2835_gpio_write(PIN1, HIGH);
			break;

		case  2:
			bcm2835_gpio_write(PIN2, HIGH);
			break;
		case  3:
			bcm2835_gpio_write(PIN3, HIGH);
			break;
		case  4:
			bcm2835_gpio_write(PIN4, HIGH);
			break;
		case  5:
			bcm2835_gpio_write(PIN5, HIGH);
			break;
		case  6:
			bcm2835_gpio_write(PIN6, HIGH);
			break;
		case  7:
			bcm2835_gpio_write(PIN7, HIGH);
			break;
		case  8:
			bcm2835_gpio_write(PIN8, HIGH);
			i = 0;
			break;
	};

}

