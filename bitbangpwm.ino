//bit bang method using delayMicroseconds()
//claims 3 microsecond is minimum for delayMicroseconds
//max is 16383 us
// As of Arduino 0018, delayMicroseconds() no longer disables interrupts. 
//https://www.arduino.cc/en/Reference/DelayMicroseconds

void setup()
{
  pinMode(13, OUTPUT);
}

void loop()
{
  digitalWrite(13, HIGH);
  delayMicroseconds(3); // Approximately 50% duty cycle @ 1KHz
  digitalWrite(13, LOW);
  delayMicroseconds(3);
}
