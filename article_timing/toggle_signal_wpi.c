#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <wiringPi.h>
#include <semaphore.h>
#include <signal.h>
#include <sys/time.h>
#include <unistd.h>

int toggle_pin = 3;
int pin_state;

static void timersignalhandler()
{
	digitalWrite( toggle_pin, pin_state );
	pin_state = !pin_state;
}

void set_periodic_timer( long delay )
{
	struct itimerval tval = {
		/* subsequent firings */ .it_interval = { .tv_sec = 0, .tv_usec = delay },
		/* first firing */       .it_value = { .tv_sec = 0, .tv_usec = delay }};
	setitimer( ITIMER_REAL, &tval, (struct itimerval*)0 );
}

int main( int argc, char** argv )
{
	int interval_us = 100;
	if (argc>1) {
		toggle_pin = atoi(argv[1]);
		if (argc>2) {
			interval_us = atoi(argv[2]);
		}
	}
	wiringPiSetup();
	pinMode( toggle_pin, OUTPUT );
	
	signal( SIGALRM, timersignalhandler );
	set_periodic_timer( interval_us );

	getchar();
	
	return 0;
}
