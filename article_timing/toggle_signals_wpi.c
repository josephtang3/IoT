#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <wiringPi.h>
#include <semaphore.h>
#include <signal.h>
#include <time.h>
#include <unistd.h>

int toggle_pin[2] = { 3, 4 };
int pin_state[2];

timer_t timer1;
timer_t timer2;

static void timersignalhandler( int sig, siginfo_t* psi, void* puc )
{
	timer_t* ptm = psi->si_value.sival_ptr;
	if (ptm==&timer1) {
		digitalWrite( toggle_pin[0], pin_state[0] );
		pin_state[0] = !pin_state[0];
	} else if (ptm==&timer2) {
		digitalWrite( toggle_pin[1], pin_state[1] );
		pin_state[1] = !pin_state[1];
	}
}

int main( int argc, char** argv )
{
	int interval_us = 100;
	if (argc>1) {
		toggle_pin[0] = atoi(argv[1]);
		if (argc>2) {
			interval_us = atoi(argv[2]);
		}
	}
	wiringPiSetup();
	pinMode( toggle_pin[0], OUTPUT );
	pinMode( toggle_pin[1], OUTPUT );
	
	struct sigaction sigact;
	sigact.sa_flags = SA_SIGINFO;
	sigact.sa_sigaction = timersignalhandler;
	sigemptyset(&sigact.sa_mask);
	if (sigaction( SIGRTMIN, &sigact, NULL )) {
		perror("sigaction");
	}
	
	
	struct sigevent sevt;
	sevt.sigev_notify = SIGEV_SIGNAL;
	sevt.sigev_signo = SIGRTMIN;
	sevt.sigev_value.sival_ptr = &timer1;
	timer_t timerid_1;
	if (timer_create( CLOCK_REALTIME, &sevt, &timerid_1 )) {
		perror("timer_create");
	}

	struct itimerspec intervaltimer;
	intervaltimer.it_value.tv_sec = intervaltimer.it_interval.tv_sec = 0U;
	intervaltimer.it_value.tv_nsec = intervaltimer.it_interval.tv_nsec = 30U * 1000UL;
	if (timer_settime( timerid_1, 0, &intervaltimer, NULL )) {
		perror("timer_settime");
	}

	struct sigevent sevt2;
	sevt2.sigev_notify = SIGEV_SIGNAL;
	sevt2.sigev_signo = SIGRTMIN;
	sevt2.sigev_value.sival_ptr = &timer2;
	timer_t timerid_2;
	if (timer_create( CLOCK_REALTIME, &sevt2, &timerid_2 )) {
		perror("timer_create");
	}
	intervaltimer.it_value.tv_nsec = intervaltimer.it_interval.tv_nsec = 150U * 1000UL;
	if (timer_settime( timerid_2, 0, &intervaltimer, NULL )) {
		perror("timer_settime");
	}

	for(;;);
	getchar();
	
	return 0;
}
