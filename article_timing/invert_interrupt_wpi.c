#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <wiringPi.h>

int pin_output = 3;
int pin_input = 6;

void pin_edge(void)
{
	digitalWrite( pin_output+1, HIGH );
	if (digitalRead( pin_input )) {
		digitalWrite( pin_output, LOW );
	} else {
		digitalWrite( pin_output, HIGH );
	}
	digitalWrite( pin_output+1, LOW );
}

int main( int argc, char** argv )
{
	wiringPiSetup();
	pinMode( pin_output, OUTPUT );
	pinMode( pin_output+1, OUTPUT );
	pinMode( pin_input, INPUT );
	wiringPiISR( pin_input, INT_EDGE_BOTH, pin_edge );
	
	digitalWrite( pin_output, (digitalRead( pin_output )) ? LOW : HIGH );
	
	getchar();
	return 0;
}
