#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>

/*From http://elinux.org/RPi_Low-level_peripherals#C */

/*Peripheral address for Raspberry-Pi model 1*/
//#define BCM2708_PERI_BASE        0x20000000

/*Peripheral address for Raspberry-Pi model 2*/
#define BCM2708_PERI_BASE        0x3F000000

#define GPIO_BASE                (BCM2708_PERI_BASE + 0x200000)

#define PAGE_SIZE (4*1024)
#define BLOCK_SIZE (4*1024)

#define INP_GPIO(g) pgpio[(g)/10] &= ~(7<<(((g)%10)*3))
#define OUT_GPIO(g) pgpio[(g)/10] |=  (1<<(((g)%10)*3))

#define GPIO_SET pgpio[7]
#define GPIO_CLR pgpio[10]

int main( void )
{
	int  mem_fd;
	void *gpio_map;

	if ((mem_fd = open("/dev/mem", O_RDWR|O_SYNC) ) < 0) {
		perror("open");
	}

	gpio_map = mmap( NULL, BLOCK_SIZE, PROT_READ|PROT_WRITE, MAP_SHARED, mem_fd, GPIO_BASE );
	
	close(mem_fd);
	if (gpio_map == MAP_FAILED) {
		perror( "mmap" );
	}
	volatile unsigned* pgpio = (volatile unsigned *)gpio_map;
	
	INP_GPIO( 22 );
	OUT_GPIO( 22 );
	
	for(;;) {
		GPIO_SET = 1<<22;
		GPIO_CLR = 1<<22;
	}

}
