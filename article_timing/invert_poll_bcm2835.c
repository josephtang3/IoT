#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <bcm2835.h>

int pin_output = 22;
int pin_input = 25;

int main( int argc, char** argv )
{
	(void)(argc);
	(void)(argv);

	if (!bcm2835_init()) {
		return 1;
	}
	
	bcm2835_gpio_fsel( pin_output, BCM2835_GPIO_FSEL_OUTP );
	bcm2835_gpio_fsel( pin_input, BCM2835_GPIO_FSEL_INPT );
	bcm2835_gpio_set_pud( pin_input, BCM2835_GPIO_PUD_DOWN );
	bcm2835_gpio_ren( pin_input );
	bcm2835_gpio_fen( pin_input );

	if (bcm2835_gpio_lev(pin_input)) {
		bcm2835_gpio_clr( pin_output );
	} else {
		bcm2835_gpio_set( pin_output );
	}

	for(;;) {
		if (bcm2835_gpio_eds(pin_input)) {
			bcm2835_gpio_set_eds( pin_input );
			if (bcm2835_gpio_lev(pin_input)) {
				bcm2835_gpio_clr( pin_output );
			} else {
				bcm2835_gpio_set( pin_output );
			}
		}
	}
}
