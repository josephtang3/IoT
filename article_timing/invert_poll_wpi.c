#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <wiringPi.h>

int pin_output = 3;
int pin_input = 6;

int main( int argc, char** argv )
{
	(void)(argc);
	(void)(argv);
	wiringPiSetup();
	pinMode( pin_output, OUTPUT );
	pinMode( pin_input, INPUT );

	for(;;) {
		digitalWrite( pin_output, !digitalRead( pin_input ) );
	}
	
	return 0;
}
