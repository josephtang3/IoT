#include <stdint.h>
#include <stdlib.h>
#include <wiringPi.h>

int toggle_pin = 3;

int main( int argc, char** argv )
{
	if (argc>1) {
		toggle_pin = atoi(argv[1]);
	}
	wiringPiSetup();
	pinMode( toggle_pin, OUTPUT );

	for(;;) {
		digitalWrite( toggle_pin, LOW );
		digitalWrite( toggle_pin, HIGH );
	}
	
	return 0;
}
