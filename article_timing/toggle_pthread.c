#include <stdint.h>
#include <pthread.h>
#include <wiringPi.h>
#include <unistd.h>

int toggle_pin[3] = { 3, 4, 5 };
int pin_state[3];

void* thread_1( void* p )
{
	struct timespec ts;
	ts.tv_sec = 0U;
	ts.tv_nsec = 100U*1000U;
	for(;;) {
		digitalWrite( toggle_pin[0], pin_state[0] );
		pin_state[0] = !pin_state[0];
		nanosleep( &ts, 0U );
	}
}

void* thread_2( void* p )
{
	struct timespec ts;
	ts.tv_sec = 0U;
	ts.tv_nsec = 150U*1000U;
	for(;;) {
		digitalWrite( toggle_pin[1], pin_state[1] );
		pin_state[1] = !pin_state[1];
		nanosleep( &ts, 0U );
	}
}

void* thread_3( void* p )
{
	struct timespec ts;
	ts.tv_sec = 0U;
	ts.tv_nsec = 10*1000U*1000U;
	for(;;) {
		digitalWrite( toggle_pin[2], pin_state[2] );
		pin_state[2] = !pin_state[2];
		nanosleep( &ts, 0U );
	}
}

int main( int argc, char** argv )
{
	wiringPiSetup();
	pinMode( toggle_pin[0], OUTPUT );
	pinMode( toggle_pin[1], OUTPUT );
	pinMode( toggle_pin[2], OUTPUT );
	
	pthread_t threads[3];
	pthread_create( &threads[0], 0, thread_1, 0 );
	pthread_create( &threads[1], 0, thread_2, 0 );
	pthread_create( &threads[2], 0, thread_3, 0 );
	
	getchar();

	return 0;
}
