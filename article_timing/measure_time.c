#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <wiringPi.h>

int pin_input = 6;

int main( void )
{
	wiringPiSetup();
	pinMode( pin_input, INPUT );
	
	unsigned int atimes[40+1];
	
	while(!digitalRead(pin_input));
	while(digitalRead(pin_input));
	
	unsigned int pos;
	for( pos=0U; pos<sizeof(atimes)/sizeof(atimes[0])/2; ++pos )
	{
		atimes[2*pos+0] = micros();
		while(!digitalRead(pin_input));
		atimes[2*pos+1] = micros();
		while(digitalRead(pin_input));
	}
	atimes[2*pos+0] = micros();
	
	for( pos=0U; pos<sizeof(atimes)/sizeof(atimes[0])/2; ++pos )
	{
		unsigned int dtL = atimes[2*pos+1]-atimes[2*pos+0];
		unsigned int dtH = atimes[2*(pos+1)+0]-atimes[2*pos+1];
		printf("dtL=%d dtH=%d\tF=%gHz\tn=%g%%\n", dtL, dtH, 1E6/(dtL+dtH), dtH*100.0/(dtL+dtH) );
	}
	
	
	return 0;
}
