#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <wiringPi.h>
#include <signal.h>
#include <time.h>

int toggle_pin[2] = { 3, -1 };

int pin_state[2] = { 0, 0 };

int intervals[2] = { 40, 100 };

void timer_event_1( union sigval my_sigval )
{
	(void)(my_sigval);
	if (pin_state[0]) {
		digitalWrite( toggle_pin[0], HIGH );
	} else {
		digitalWrite( toggle_pin[0], LOW );
	}
	pin_state[0] = !pin_state[0];
}

void timer_event_2( union sigval my_sigval )
{
	(void)(my_sigval);
	if (pin_state[1]) {
		digitalWrite( toggle_pin[1], HIGH );
	} else {
		digitalWrite( toggle_pin[1], LOW );
	}
	pin_state[1] = !pin_state[1];
}

int main( int argc, char** argv )
{
	if (argc>1) {
		toggle_pin[0] = atoi(argv[1]);
		if (argc>2) {
			toggle_pin[1] = atoi(argv[2]);
			if (argc>3) {
				intervals[0] = atoi(argv[3]);
				if (argc>4) {
					intervals[1] = atoi(argv[3]);
				}
			}
		}
	}

	wiringPiSetup();
	pinMode( toggle_pin[0], OUTPUT );
	if (toggle_pin[1]>=0) {
		pinMode( toggle_pin[1], OUTPUT );
	}
	
	struct sigevent sevt;
	memset( &sevt, 0, sizeof(sevt) );
	sevt.sigev_notify = SIGEV_THREAD;
	sevt.sigev_notify_function = timer_event_1;
	timer_t timerid_1;
	if (timer_create( CLOCK_REALTIME, &sevt, &timerid_1 )) {
		fprintf(stderr,"Failed to create timer!\n");
		return 1;
	}
	
	timer_t timerid_2;
	if (toggle_pin[1]>=0) {
		sevt.sigev_notify_function = timer_event_2;
		if (timer_create( CLOCK_REALTIME, &sevt, &timerid_2 )) {
			fprintf(stderr,"Failed to create timer!\n");
			return 1;
		}
	}
	
	struct itimerspec intervaltimer;
	intervaltimer.it_value.tv_sec = intervaltimer.it_interval.tv_sec = 0U;
	intervaltimer.it_value.tv_nsec = intervaltimer.it_interval.tv_nsec = intervals[0] * 1000UL;
	if (timer_settime( timerid_1, 0, &intervaltimer, NULL )) {
		fprintf(stderr,"Settime failed!\n");
	}

	if (toggle_pin[1]>=0 && intervals[1]>0) {
		struct itimerspec intervaltimer2;
		intervaltimer2.it_value.tv_sec = intervaltimer2.it_interval.tv_sec = 0U;
		intervaltimer2.it_value.tv_nsec = intervaltimer2.it_interval.tv_nsec = intervals[1] * 1000UL;
		if (timer_settime( timerid_2, 0, &intervaltimer2, NULL )) {
			fprintf(stderr,"Settime failed!\n");
		}
	}

	getchar();
	
	return 0;
}
