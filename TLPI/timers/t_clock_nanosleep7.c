/* t_clock_nanosleep7.c

   See also t_nanosleep.c.

   Linux supports clock_nanosleep() since kernel 2.6.

*/

#if ! defined(_XOPEN_SOURCE) || _XOPEN_SOURCE < 600
#define _XOPEN_SOURCE 600
#endif
#include <sys/time.h>
#include <time.h>
#include <signal.h>
#include "tlpi_hdr.h"
#include "error_functions.h"
#include <stdio.h>
#include <wiringPi.h>
#include <semaphore.h>
#include <errno.h>

    struct timeval start, finish;
    struct timespec request, remain;
    struct sigaction sa;
    int s, flags;
    int cycles = 0;
    int  sleep_stop;



static void
sigintHandler(int sig)
{
    printf("caught signal, sig = %i\n", sig);
    return;                             /* Just interrupt clock_nanosleep() */
}


int go_nanosleep( struct timespec request, int nsec, int flags, int volt){

//    request.tv_sec = 0;
    request.tv_nsec += nsec;
//    request.tv_nsec = 1000000;//just for testing

    printf("inside go_nanosleep, request.tv_nsec = %ld\n", request.tv_nsec);

        printf("Initial CLOCK_MONOTONIC value: %ld.%09ld\n",
                (long) request.tv_sec, (long) request.tv_nsec);


    if (gettimeofday(&start, NULL) == -1)
        perror("gettimeofday");

    sleep_stop = 1;

    while (sleep_stop !=0){

        digitalWrite(4,volt);
//        s = clock_nanosleep(CLOCK_REALTIME, flags, &request, NULL);//set &remain to NULL
        s = clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &request, NULL);//set &remain to NULL, flags to TIMER_ABSTIME

        printf("in the loop\n");

       if (s != 0 && s != EINTR)
            perror("clock_nanosleep ");

        if (s == EINTR)
            printf("Interrupted... ");

        if (gettimeofday(&finish, NULL) == -1)
            perror("gettimeofday");

        printf("Slept: %.6f secs", finish.tv_sec - start.tv_sec +
                        (finish.tv_usec - start.tv_usec) / 1000000.0);

        if (s == 0){
             sleep_stop = 0;
             break;                      /* sleep completed */
        }

        if (flags != TIMER_ABSTIME) {
            printf("... Remaining: %ld.%09ld", (long) remain.tv_sec, remain.tv_nsec);
            //request = remain;
        }

        printf("... Restarting\n");

    }//while
    sleep_stop = 1;

    printf("\nSleep complete\n");

    return;
}

int
main(int argc, char *argv[]){

    wiringPiSetup();
    pinMode (4,OUTPUT);

    int nsec;

    if (argc < 3 || strcmp(argv[1], "--help") == 0)
        printf("%s secs nanosecs [a]\n", argv[0]);

    /* Allow SIGINT handler to interrupt clock_nanosleep() */

    sigemptyset(&sa.sa_mask);
    sa.sa_flags = 0;
    sa.sa_handler = sigintHandler;
    if (sigaction(SIGINT, &sa, NULL) == -1)
        perror("sigaction");

    /* If more than three command-line arguments, use TIMER_ABSTIME flag */

    flags = (argc > 3) ? TIMER_ABSTIME : 0;

    if (flags == TIMER_ABSTIME) {
        if (clock_gettime(CLOCK_MONOTONIC, &request) == -1)
            perror("clock_gettime");

        printf("Initial CLOCK_MONOTONIC value: %ld.%09ld\n",
                (long) request.tv_sec, (long) request.tv_nsec);
    }

    printf("argv[1] = %d\n", (long)atoi(argv[1]));
    printf("argv[2] = %d\n", (long)atoi(argv[2]));


//    for(cycles=0;cycles<7;cycles++){

//        digitalWrite(4,HIGH);//wrong place ?
//        delay(2);//testing >> no use
        //request.tv_sec  += getLong(argv[1], 0, "secs");
        //request.tv_nsec += getLong(argv[2], 0, "nanosecs");


//        request.tv_sec += (long)atoi(argv[1]);
//        request.tv_nsec += (long)atoi(argv[2]);

        nsec = (long)atoi(argv[2]);


    for(cycles=0;cycles<100;cycles++){

        if (clock_gettime(CLOCK_MONOTONIC, &request) == -1)
            perror("clock_gettime");//get current time

        go_nanosleep(request, nsec, flags, 1);//high

        if (clock_gettime(CLOCK_MONOTONIC, &request) == -1)
            perror("clock_gettime");//get current time

        printf("Initial CLOCK_MONOTONIC value: %ld.%09ld\n",
                (long) request.tv_sec, (long) request.tv_nsec);

        go_nanosleep(request, nsec, flags, 0);//low
    }


//                    request.tv_sec = 10;
//                    request.tv_nsec = 0;

//        if (request.tv_nsec >= 1000000000) {
//            request.tv_sec += request.tv_nsec / 1000000000;
//            request.tv_nsec %= 1000000000;
//        } else 
//         {                    /* Relative sleep */

            //request.tv_sec  = getLong(argv[1], 0, "secs");
           //request.tv_nsec = getLong(argv[2], 0, "nanosecs");
//              request.tv_sec = (long)argv[1];
//              request.tv_nsec = (long)argv[2];
//                    request.tv_sec = 0;
//                    request.tv_nsec = 100000;


//        }

//        digitalWrite(4,LOW); //wrong place ??
//        delay(2);//testing >>  no use
        //request.tv_sec  += getLong(argv[1], 0, "secs");
        //request.tv_nsec += getLong(argv[2], 0, "nanosecs");
//        request.tv_sec += (long)argv[1];
//        request.tv_nsec += (long)argv[2];
//                    request.tv_sec = 0;
//                  request.tv_nsec = 10000;


//        if (request.tv_nsec >= 1000000000) {
//            request.tv_sec += request.tv_nsec / 1000000000;
//          request.tv_nsec %= 1000000000;
 //       } else {                    /* Relative sleep */

                  //request.tv_sec  = getLong(argv[1], 0, "secs");
                  //request.tv_nsec = getLong(argv[2], 0, "nanosecs");
//                    request.tv_sec += (long)argv[1];
//                    request.tv_nsec += (long)argv[2];
//                    request.tv_sec = 0;
//                    request.tv_nsec = 10000;
//         } 

//    }//for





    exit(EXIT_SUCCESS);

}
