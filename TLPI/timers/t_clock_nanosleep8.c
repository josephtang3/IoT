/* t_clock_nanosleep8.c

 this version do not use go_nanosleep() function

   See also t_nanosleep.c.

   Linux supports clock_nanosleep() since kernel 2.6.

int clock_nanosleep(clockid_t clockid, int flags, 
    const struct timespec *request, struct timespec * remain)

returns 0 on successfully completed sleep,
or a positive error number on error or interrupted sleep


When the flag TIMER_ABSTIME is set in the flags argument,
the clock_nanosleep() function shall cause the current thread
to be suspended from execution until either:
-  the time value of the clock specified by the clockid reaches
   the absolute time specified by the request argument,
-  or a signal is delivered to the calling and its action is to
   invoke a signal-catching function, or the process is terminated


If, at the time of the call, the time value specified by rqtp is less
than or equal to the time value of the specified clock, then 
clock_nanosleep() shall return immediately and the calling process 
shall not be suspended.

To avoid oversleeping problem:
-  make an initial call to clock_gettime() // 1 // to retrieve the time,
-  adding the desired amount to that time, // 2 //
-  and then calling clock_nanosleep() with the TIMER_ABSTIME flag // 3 //
-  ( and restarting the system call if it is interrupted by a signal handler)
LPTI p.494

fix invalid argument bug, which is carry-over. Add lines 223~5, 294~296 for request.tv_nsec

*/

#if ! defined(_XOPEN_SOURCE) || _XOPEN_SOURCE < 600
#define _XOPEN_SOURCE 600
#endif
#include <sys/time.h>
#include <time.h>
#include <signal.h>
#include "tlpi_hdr.h"
#include "error_functions.h"
#include <stdio.h>
#include <stdlib.h>
#include <wiringPi.h>
#include <semaphore.h>
#include <errno.h>
#include <unistd.h>

    struct timeval start, finish;
    struct timespec request, remain;
    struct sigaction sa;
    int s, flags;
    int cycles = 0;
    int c;
    static volatile int  sleep_stop;//global flag



static void
sigintHandler(int signal)
{
    const char *signal_name;
    sigset_t pending;

//    printf("caught signal, signal = %i\n", signal);

    /* find out which signal we're handling */
    switch (signal) {
        case SIGHUP:
            signal_name = "SIGHUP";
            break;
        case SIGUSR1:
            signal_name = "SIGUSR1";
            break;
        case SIGINT:
            signal_name = "SIGINT";
            exit(0);

        default:
        fprintf(stderr, "Caught unintended signal: %d\n", signal);
        return;
    }//switch

    printf("sigintHandler caught %s\n", signal_name);

}


int go_nanosleep( struct timespec request, int nsec, int flags, int volt){

//    request.tv_sec = 0;
    request.tv_nsec += nsec; // 2 //

    if(request.tv_nsec >= 1000000000) {
        request.tv_sec += request.tv_nsec / 1000000000;
        request.tv_nsec %= 1000000000;
    }

    printf("inside go_nanosleep, request.tv_nsec = %ld\n", request.tv_nsec);

        printf("Initial CLOCK_MONOTONIC value: %ld.%09ld\n",
                (long) request.tv_sec, (long) request.tv_nsec);


    if (gettimeofday(&start, NULL) == -1)
        perror("gettimeofday");

    sleep_stop = 1;

    while (sleep_stop !=0){

        digitalWrite(4,volt);
//        s = clock_nanosleep(CLOCK_REALTIME, flags, &request, NULL);//set &remain to NULL
        s = clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &request, NULL);//set &remain to NULL, flags to TIMER_ABSTIME // 1,3 //

        printf("in the loop\n");

       if (s != 0 && s != EINTR)
            perror("!!clock_nanosleep!! ");

        if (s == EINTR)
            printf("Interrupted... ");

        if (gettimeofday(&finish, NULL) == -1)
            perror("gettimeofday");

//        printf("Slept: %.6f secs", finish.tv_sec - start.tv_sec +
//                        (finish.tv_usec - start.tv_usec) / 1000000.0);

        if (s == 0){
             sleep_stop = 0;
             break;                      /* sleep completed */
        }

        if (flags != TIMER_ABSTIME) {
            printf("... Remaining: %ld.%09ld", (long) remain.tv_sec, remain.tv_nsec);
            //request = remain;
        }

        printf("... Restarting go_nano\n");

    }//while
    sleep_stop = 1;

//    printf("\nSleep complete go_nano_sleep\n");

    return;
}

int main(int argc, char *argv[]){

    wiringPiSetup();
    pinMode (4,OUTPUT);

    int nsec;

    if (argc < 3 || strcmp(argv[1], "--help") == 0)
        printf("%s secs nanosecs cycles [a]\n", argv[0]);

    /* Print pid */
    printf("pid is %d\n", getpid());

    /* Allow SIGINT handler to interrupt clock_nanosleep() */

    sigemptyset(&sa.sa_mask);
    sa.sa_flags = 0;
    sa.sa_handler = sigintHandler;
    //Block every signal during the handler
    sigfillset(&sa.sa_mask);

    if (sigaction(SIGINT, &sa, NULL) == -1)
        perror("Error: cannot handle SIGINT");//Should not happen

    if (sigaction(SIGHUP, &sa, NULL) == -1)
        perror("Error: cannot handle SIGHUP");//Should not happen

    if (sigaction(SIGUSR1, &sa, NULL) == -1)
        perror("Error: cannot handle SIGUSR1");//Should not happen

//    if (sigaction(SIGKILL, &sa, NULL) == -1)
//        perror("Error: cannot handle SIGKILL");//will always happen

    /* If more than three command-line arguments, use TIMER_ABSTIME flag */

    flags = (argc > 3) ? TIMER_ABSTIME : 0;

    if (flags == TIMER_ABSTIME) {
        if (clock_gettime(CLOCK_MONOTONIC, &request) == -1)
            perror("clock_gettime"); // 1,3 //

        printf("Initial CLOCK_MONOTONIC value: %ld.%09ld\n",
                (long) request.tv_sec, (long) request.tv_nsec);
    }

    printf("argv[1] = %d\n", (long)atoi(argv[1]));
    printf("argv[2] = %d\n", (long)atoi(argv[2]));


    digitalWrite(4,LOW);//reset output to LOW
//    sleep(3);//just for testing

    nsec = (long)atoi(argv[2]);
    cycles = (int)atol(argv[3]);

    for(c=0;c<cycles;c++){// cycles


    /* high */

        if (clock_gettime(CLOCK_MONOTONIC, &request) == -1)
            perror("clock_gettime");//get current time // 1,3 //

        request.tv_sec += 0;
        request.tv_nsec += nsec; // 2 //

        if(request.tv_nsec >= 1000000000) {
            request.tv_sec += request.tv_nsec / 1000000000;
            request.tv_nsec %= 1000000000;
        }

//        printf("request.tv_nsec = %ld\n", request.tv_nsec);

//        printf("Initial CLOCK_MONOTONIC value: %ld.%09ld\n",
//                (long) request.tv_sec, (long) request.tv_nsec);


        if (gettimeofday(&start, NULL) == -1)
            perror("gettimeofday &start");

    
        sleep_stop = 1;
        digitalWrite(4, HIGH);

        while (sleep_stop != 0){
//        s = clock_nanosleep(CLOCK_REALTIME, flags, &request, NULL);//set &remain to NULL
            s = clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &request, NULL);//set &remain to NULL, flags to TIMER_ABSTIME


            if(s!= 0){
               printf("s not zero,  = %i\n",s);
            }

            if(s!=0 && s==22){
                perror("error 22 !!");
                break;//to break for unknown 22
            }

            if (s!=0 && s != EINTR)
                perror("!!clock_nanosleep!!");

            if (s == EINTR)
                perror("Interrupted... ");

            if(s==EINVAL)
                perror("EINVAL");

            if (gettimeofday(&finish, NULL) == -1)
                perror("gettimeofday &finish");

//            printf("Slept: %.6f secs", finish.tv_sec - start.tv_sec +
//                        (finish.tv_usec - start.tv_usec) / 1000000.0);

            if (s == 0){
                sleep_stop = 0;
                 break;                      /* sleep completed */
            }

            if (flags != TIMER_ABSTIME) {
                printf("... Remaining: %ld.%09ld", (long) remain.tv_sec, remain.tv_nsec);
                //request = remain;
            }

            printf("... Restarting high\n");
 
    }//while

//    printf("\nSleep complete HIGH\n");

    /* low */

    if (clock_gettime(CLOCK_MONOTONIC, &request) == -1)
        perror("clock_gettime &request");//get current time // 1,3 //

    request.tv_sec += 0;
    request.tv_nsec += nsec; // 2 //

    if(request.tv_nsec >= 1000000000) {
            request.tv_sec += request.tv_nsec / 1000000000;
            request.tv_nsec %= 1000000000;
    }


//    printf("request.tv_nsec = %ld\n", request.tv_nsec);

//    printf("Initial CLOCK_MONOTONIC value: %ld.%09ld\n",
//            (long) request.tv_sec, (long) request.tv_nsec);


    if (gettimeofday(&start, NULL) == -1)
        perror("gettimeofday &start");

    digitalWrite(4, LOW);

    sleep_stop = 1;

    while (sleep_stop !=0){
//        s = clock_nanosleep(CLOCK_REALTIME, flags, &request, NULL);//set &remain to NULL
        s = clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &request, NULL);//set &remain to NULL, flags to TIMER_ABSTIME
        if(s!= 0){
           printf("s not zero,  = %i\n",s);
        }

        if (s != 0 && s != EINTR)
            perror("!!clock_nanosleep!!");

        if (s == EINTR)
            printf("Interrupted... ");

        if (gettimeofday(&finish, NULL) == -1)
            perror("gettimeofday &finish");

//        printf("Slept: %.6f secs", finish.tv_sec - start.tv_sec +
//                        (finish.tv_usec - start.tv_usec) / 1000000.0);

        if (s == 0){
             sleep_stop = 0;
             break;                      /* sleep completed */
        }

        if (flags != TIMER_ABSTIME) {
            printf("... Remaining: %ld.%09ld", (long) remain.tv_sec, remain.tv_nsec);
            //request = remain;
        }

        printf("... Restarting low\n");

    }//while

    sleep_stop = 1;
//    printf("\nSleep complete LOW\n");

}//for cycles

    exit(EXIT_SUCCESS);

}
