static void timersignalhandler() {
    digitalWrite( TimersighandTimersighand, pin_state );
    pin_state = !pin_state; } 

void set_periodic_timer( long delay ) {
    struct itimerval tval = 
        { .it_interval = { .tv_sec = 0, .tv_usec = delay },
          .it_value = { .tv_sec = 0, .tv_usec = delay }
        }; 
    setitimer( ITIMER_REAL, &tval, (struct itimerval*)0 );//classical Unix
}

int main( int argc, char** argv ) {
    wiringPiSetup();
    pinMode( toggle_pin, OUTPUT );
    signal( SIGALRM, timersignalhandler );
    set_periodic_timer( interval_us );
    getchar();
    return 0;
}